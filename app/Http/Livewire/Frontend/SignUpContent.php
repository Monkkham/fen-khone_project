<?php

namespace App\Http\Livewire\Frontend;

use App\Models\User;
use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class SignUpContent extends Component
{
    public $name_lastname, $phone, $gender,$email, $status, $password,$confirmPassword;
    public function render()
    {
        return view('livewire.frontend.sign-up-content')->layout('layouts.frontend.style');
    }
    public function SignUp()
    {
        $this->validate([
            'name_lastname' => 'required',
            'phone' => 'required|min:8|max:8|unique:users',
            'password' => 'required|min:6',
            'confirmPassword' => 'required|same:password',
        ], [
            'name_lastname.required' => 'ປ້ອນຊື່ ນາມສະກຸນກ່ອນ!',
            'phone.required' => 'ປ້ອນເບີໂທກ່ອນ!',
            'phone.unique' => 'ເບີໂທນີ້ມີໃນລະບົບເເລ້ວ!',
            'phone.min' => 'ເບີໂທ8ໂຕເລກເທົ່ານັ້ນ!',
            'phone.max' => 'ເບີໂທ8ໂຕເລກເທົ່ານັ້ນ!',
            'password.required' => 'ປ້ອນລະຫັດຜ່ານກ່ອນ!',
            'password.min' => 'ລະຫັດ6ຕົວຂື້ນໄປ!',
            'confirmPassword.required' => 'ປ້ອນຍືນຍັນລະຫັດກ່ອນ!',
            'confirmPassword.same' => 'ລະຫັດຜ່ານ ເເລະ ຍືນຍັນລະຫັດບໍ່ຕົງກັນ!',
        ]);
        $data = new User();
        $data->name_lastname = $this->name_lastname;
        $data->phone = $this->phone;
        $data->password = Hash::make($this->password);
        // $data->gender = $this->gender ?? '';
        // $data->email = $this->email ?? '';
        // $data->status = $this->status ?? '';
        $data->roles_id = 3;
        // $data->doc_type = 3;
        // $data->name_doc = '';
        // $data->number_doc = '';
        $data->save();
        Auth::guard('admin')->login($data);
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ລົງທະບຽນສຳເລັດເເລ້ວ!',
            'icon' => 'success',
        ]);
        return redirect(route('frontend.home'));
    }
}
