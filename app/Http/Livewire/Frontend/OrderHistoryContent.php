<?php

namespace App\Http\Livewire\Frontend;

use App\Models\Sales;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class OrderHistoryContent extends Component
{
    public $ID, $note;
    public function render()
    {
        $data = Sales::withSum('sales_detail', 'subtotal')->where('customer_id', auth()->user()->id)->get();
        return view('livewire.frontend.order-history-content', compact('data'))->layout('layouts.frontend.style');
    }
    public function show_cancle($ids)
    {
        $data = Sales::find($ids);
        $this->ID = $data->id;
        $this->note = $data->note;
        $this->dispatchBrowserEvent('show-modal-delete');
    }
    public function cancle($ids)
    {
        $this->validate([
            'note' => 'required',
        ], [
            'note.required' => 'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
        ]);
        try {
            DB::beginTransaction();
            $data = Sales::find($ids);
            $data->note = $this->note;
            $data->status = 4;
            $data->save();
            $this->dispatchBrowserEvent('hide-modal-delete');
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ຍົກເລີກສຳເລັດເເລ້ວ!',
                'icon' => 'success',
            ]);
            DB::commit();
        } catch (\Exception $ex) {
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ມີບາງຢ່າງຜິດພາດ!',
                'icon' => 'error',
            ]);
        }
    }
}
