<?php

namespace App\Http\Livewire\Frontend;

use App\Models\District;
use App\Models\Product;
use App\Models\Province;
use App\Models\Sales;
use App\Models\SalesDetail;
use App\Models\ShopCart;
use App\Models\User;
use App\Models\Village;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Livewire\WithFileUploads;

class CheckOutContent extends Component
{
    public $name_lastname,
    $phone,
    $email,
    $gender,
    $province_id,
    $village_id,
    $district_id,
    $name_doc,
    $number_doc,
    $doc_type,
    $districts = [],
    $villages = [];

    public $search_product, $search, $branchs, $sum_subtotal, $qty, $customer_id, $customer_data, $type = 1, $onepay, $note, $product_type_id;
    use WithFileUploads;

    public function mount()
    {
        $user = User::where('id', auth()->user()->id)->first();
        $this->name_lastname = $user->name_lastname;
        $this->phone = $user->phone;
        $this->email = $user->email;
        $this->gender = $user->gender;
        $this->province_id = $user->province_id;
        $this->district_id = $user->district_id;
        $this->village_id = $user->village_id;
        $this->name_doc = $user->name_doc;
        $this->number_doc = $user->number_doc;
    }

    public function render()
    {
        $province = Province::all();
        if ($this->province_id) {
            $this->districts = District::where('province_id', $this->province_id)->get();
        }
        if ($this->district_id) {
            $this->villages = Village::where('district_id', $this->district_id)->get();
        }
        $shop_cart = ShopCart::where('creator_id', auth()->user()->id)->get();
        $sum_subtotals = $shop_cart->sum('subtotal');
        return view('livewire.frontend.check-out-content', compact('province', 'shop_cart', 'sum_subtotals'))->layout('layouts.frontend.style');
    }
    public function resetField()
    {
    }
    public function updateProfile()
    {
        $data = User::find(auth()->user()->id);
        $data->name_lastname = $this->name_lastname;
        $data->phone = $this->phone;
        $data->email = $this->email;
        $data->gender = $this->gender;
        $data->province_id = $this->province_id;
        $data->district_id = $this->district_id;
        $data->village_id = $this->village_id;
        $data->doc_type = $this->doc_type;
        $data->name_doc = $this->name_doc;
        $data->number_doc = $this->number_doc;
        $data->save();
    }
    public function PlaceSales()
    {
        $this->validate([
            'name_lastname' => 'required',
            'phone' => 'required',
            'gender' => 'required',
            'province_id' => 'required',
            'district_id' => 'required',
            'village_id' => 'required',
            'doc_type' => 'required',
            'name_doc' => 'required',
            'number_doc' => 'required',
        ], [
            'name_lastname.required' => 'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'phone.required' => 'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'gender.required' => 'ເລືອກຂໍ້ມູນກ່ອນ!',
            'province_id.required' => 'ເລືອກຂໍ້ມູນກ່ອນ!',
            'district_id.required' => 'ເລືອກຂໍ້ມູນກ່ອນ!',
            'village_id.required' => 'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'doc_type.required' => 'ເລືອກຂໍ້ມູນກ່ອນ!',
            'name_doc.required' => 'ປ້ອນຂໍ້ມູນກ່ອນ!',
            'number_doc.required' => 'ປ້ອນຂໍ້ມູນກ່ອນ!',
        ]);
        if ($this->type == 2) {
            $this->validate([
                'onepay' => 'required',
            ], [
                'onepay.required' => 'ໃສ່ຫຼັກຖານການໂອນກ່ອນ!',
            ]);
        }
        $sum_subtotal = ShopCart::select('subtotal')->where('creator_id', auth()->user()->id)->sum('subtotal');
        $outOfStockProducts = [];
        $MorethanOfStockProducts = [];
        $SshopCart = ShopCart::where('creator_id', auth()->user()->id)->get();
        foreach ($SshopCart as $key => $cart_item) {
            $check_product = Product::find($cart_item->product_id);
            if ($check_product->stock === null) {
                // Log the error or add to a list of missing products
                $this->dispatchBrowserEvent('swal', [
                    'title' => 'ຈຳນວນເກີນສະຕ໋ອກ: ' . $$cart_item->product_id,
                    'icon' => 'warning',
                ]);
                continue; // Skip to the next cart item
            }
            if ($check_product->stock < $cart_item->qty) {
                $MorethanOfStockProducts[] = $check_product->name;
            } else if ($check_product->stock <= 0) {
                $outOfStockProducts[] = $check_product->name;
            }
        }

        if (!empty($MorethanOfStockProducts)) {
            $MorethanOfStockProductsList = implode(", ", $MorethanOfStockProducts);
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ຈຳນວນເກີນສະຕ໋ອກ: ' . $MorethanOfStockProductsList,
                'icon' => 'warning',
            ]);
        } elseif (!empty($outOfStockProducts)) {
            $outOfStockProductsList = implode(", ", $outOfStockProducts);
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ໝົດສະຕ໋ອກ: ' . $outOfStockProductsList,
                'icon' => 'warning',
            ]);
        } else {
            try {
                DB::beginTransaction();
                $sales = new Sales();
                $sales->code = 'SL-' . rand(100000, 999999);
                $sales->customer_id = auth()->user()->id;
                $sales->employee_id = auth()->user()->id;
                $sales->total = $sum_subtotal;
                $sales->status = 1;
                $sales->type = $this->type;
                if (!empty($this->onepay)) {
                    $this->validate([
                        'onepay' => 'required|mimes:jpg,png,jpeg',
                    ]);
                    $imageName = Carbon::now()->timestamp . '.' . $this->onepay->extension();
                    $this->onepay->storeAs('upload/onepay', $imageName);
                    $sales->onepay = 'upload/onepay' . '/' . $imageName;
                } else {
                    $sales->onepay = '';
                }
                $sales->note = $this->note;
                $sales->type_sale = 2;
                $sales->save();
                foreach ($SshopCart as $key => $cart_item) {
                    $check_product = Product::find($cart_item->product_id);
                    if ($check_product->stock >= $cart_item->qty) {
                        $products = array(
                            'sales_id' => $sales->id,
                            'products_id' => $cart_item->product_id,
                            'sell_price' => $cart_item->price,
                            'stock' => $cart_item->qty,
                            'subtotal' => ($cart_item->price * $cart_item->qty),
                            'color' => $cart_item->color,
                            'size' => $cart_item->size,
                        );
                        $SalesDetail = SalesDetail::insert($products);
                        $clear_cart = ShopCart::where('id', $cart_item->id)->where('creator_id', auth()->user()->id)->delete();
                        if ($check_product) {
                            $check_product->stock -= $cart_item->qty; //ຕັດສະຕ່ອກ
                            // $check_product->check_2 = null;
                            $check_product->save();
                        }
                        // $this->dispatchBrowserEvent('swal', [
                        //     'title' => 'ສັ່ງຊື້ສຳເລັດເເລ້ວ!',
                        //     'icon' => 'success',
                        // ]);
                        $this->dispatchBrowserEvent('swal:login', [
                            'type' => 'success',  
                            'message' => 'ຂອບໃຈທີ່ອຸດຫນູນພວກເຮົາ! (' . auth()->user()->name_lastname . ')',
                            // 'message' => 'ຍິນດີຕ້ອນຮັບ!', 
                            'text' => 'ສັ່ງຊື້ສຳເລັດເເລ້ວ!'
                        ]);
                        $this->dispatchBrowserEvent('hide-modal-sales');
                        $this->resetField();
                    }
                }
                $this->updateProfile();
                DB::commit();
                return redirect(route('frontend.OrderHistory'));
            } catch (\Exception $ex) {
                DB::rollBack();
                dd($ex->getMessage());
                $this->emit('alert', ['type' => 'error', 'message' => 'ມີບາງຢ່າງຜິດພາດ!']);
            }
        }
    }
}
