<?php

namespace App\Http\Livewire\Frontend;

use App\Models\Product;
use Livewire\Component;
use App\Models\ShopCart;
use App\Models\WishLists;
use Illuminate\Support\Facades\DB;

class ProductDetailContent extends Component
{
    public $name,$sell_price,$note,$image,$slug_id,$sizes = 'XS',$colors = 'ດຳ';
    public $promotion_price,$stock;
    public function mount($slug_id)
    {
        $products = Product::find($slug_id);
        $this->name = $products->name;
        $this->sell_price = $products->sell_price;
        $this->promotion_price = $products->promotion_price;
        $this->note = $products->note;
        $this->image = $products->image;
        $this->name = $products->name;
        $this->stock = $products->stock;
    }
    public function render()
    {
        $products = Product::orderBy('id','desc')->get();
        return view('livewire.frontend.product-detail-content',compact('products'))->layout('layouts.frontend.style');
    }
    public function ProductDetail($slug_id)
    {
        return redirect(route('frontend.ProductDetails', $slug_id));
    }
    public function AddToCart($ids)
    {
        try {
            DB::beginTransaction();
            // Check if the product is already in the cart for the current user
            $existingCartItem = ShopCart::where('creator_id', auth()->user()->id)
                ->where('product_id', $ids)
                ->first();
            if ($existingCartItem) {
                // If the product is already in the cart, you can handle it accordingly
                $this->dispatchBrowserEvent('swal', [
                    'title' => 'ສິນຄ້າມີໃນກະຕ່າເເລ້ວ!',
                    'icon' => 'warning',
                ]);
            } else {
                $product = Product::find($ids);
                // $check_product = Product::where('id', $ids)->update(['check_shop' => 1]);

                $shop_cart = new ShopCart();
                $shop_cart->creator_id = auth()->user()->id;
                $shop_cart->product_id = $product->id;
                $shop_cart->name = $product->name;
                $shop_cart->price = $product->buy_price;
                $shop_cart->qty = 1;
                $shop_cart->subtotal = $shop_cart->price * $shop_cart->qty;
                $shop_cart->size = $this->sizes;
                $shop_cart->color = $this->colors;
                $shop_cart->save();

                $this->dispatchBrowserEvent('swal', [
                    'title' => 'ເພີ່ມໃສ່ກະຕ່າເເລ້ວ!',
                    'icon' => 'success',
                    'iconColor' => 'green',
                ]);
                return redirect(route('frontend.ProductDetails', $this->slug_id));

            }
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ເຂົ້າສູ່ລະບົບກ່ອນ!',
                'icon' => 'warning',
            ]);
        }
    }
    public function AddToWishList($ids)
    {
        try {
            DB::beginTransaction();
            // Check if the product is already in the cart for the current user
            $existingCartItem = WishLists::where('creator_id', auth()->user()->id)
                ->where('product_id', $ids)
                ->first();
            if ($existingCartItem) {
                // If the product is already in the cart, you can handle it accordingly
                $this->dispatchBrowserEvent('swal', [
                    'title' => 'ສິນຄ້າມີໃນສິ່ງທີ່ມັກເເລ້ວ!',
                    'icon' => 'warning',
                ]);
            } else {
                $product = Product::find($ids);
                // $check_product = Product::where('id', $ids)->update(['check_shop' => 1]);
                $shop_cart = new WishLists();
                $shop_cart->creator_id = auth()->user()->id;
                $shop_cart->product_id = $product->id;
                $shop_cart->name = $product->name;
                $shop_cart->price = $product->buy_price;
                $shop_cart->save();

                $this->dispatchBrowserEvent('swal', [
                    'title' => 'ເພີ່ມໃສ່ສິ່ງທີ່ມັກເເລ້ວ!',
                    'icon' => 'success',
                    'iconColor' => 'green',
                ]);
                return redirect(route('frontend.ProductDetails', $this->slug_id));
            }
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ເຂົ້າສູ່ລະບົບກ່ອນ!',
                'icon' => 'warning',
            ]);
        }
    }
}
