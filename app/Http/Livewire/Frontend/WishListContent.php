<?php

namespace App\Http\Livewire\Frontend;

use App\Models\Product;
use Livewire\Component;
use App\Models\ShopCart;
use App\Models\WishLists;
use Illuminate\Support\Facades\DB;

class WishListContent extends Component
{
    public $sizes = 'XS',$colors = 'ດຳ';

    public function render()
    {
        $wishLists = WishLists::where('creator_id',auth()->user()->id)->get();
        return view('livewire.frontend.wish-list-content',compact('wishLists'))->layout('layouts.frontend.style');
    }
    public function Remove_Item($id)
    {
        $shop_cart = WishLists::find($id);
        $shop_cart->delete();
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ລຶບຂໍ້ມູນສຳເລັດ!',
            'icon' => 'success',
        ]);
        return redirect(route('frontend.WishList'));
    }
    public function AddToCart($ids)
    {
        // try {
        //     DB::beginTransaction();
            // Check if the product is already in the cart for the current user
            $existingCartItem = ShopCart::where('creator_id', auth()->user()->id)
                ->where('product_id', $ids)
                ->first();
            if ($existingCartItem) {
                // If the product is already in the cart, you can handle it accordingly
                $this->dispatchBrowserEvent('swal', [
                    'title' => 'ສິນຄ້າມີໃນກະຕ່າເເລ້ວ!',
                    'icon' => 'warning',
                ]);
            } else {
                $WishLists = WishLists::find($ids);
                // $check_product = Product::where('id', $ids)->update(['check_shop' => 1]);

                $shop_cart = new ShopCart();
                $shop_cart->creator_id = auth()->user()->id;
                $shop_cart->product_id = $WishLists->id;
                $shop_cart->name = $WishLists->product->name ?? '';
                $shop_cart->price = $WishLists->product->buy_price ?? '';
                $shop_cart->qty = 1;
                $shop_cart->subtotal = $shop_cart->price * $shop_cart->qty;
                $shop_cart->size = $this->sizes;
                $shop_cart->color = $this->colors;
                $shop_cart->save();
                $this->Remove_Item($ids);
                $this->dispatchBrowserEvent('swal', [
                    'title' => 'ເພີ່ມໃສ່ກະຕ່າເເລ້ວ!',
                    'icon' => 'success',
                    'iconColor' => 'green',
                ]);
                return redirect(route('frontend.WishList'));
            }
            DB::commit();
        // } catch (\Exception $ex) {
        //     DB::rollBack();
        //     $this->dispatchBrowserEvent('swal', [
        //         'title' => 'ເຂົ້າສູ່ລະບົບກ່ອນ!',
        //         'icon' => 'warning',
        //     ]);
        // }
    }
}
