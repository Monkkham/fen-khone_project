<div>
    <!-- Footer Start -->
    <div class="container-fluid text-secondary mt-5 pt-5" style="background-color: {{ !empty($about->f_sidebar_color) ? $about->f_sidebar_color : '' }}">
        <div class="row px-xl-5 pt-5">
            <div class="col-lg-4 col-md-12 mb-5 pr-3 pr-xl-5">
                <h5 class="text-secondary text-uppercase mb-4">
                    @if (!empty($about))
                        <i class="fas fa-store-alt"></i> {{ $about->name_la }}
                    @endif
                </h5>
                <p class="mb-4">
                    @if (!empty($about))
                        {!! $about->note !!}
                    @endif
                </p>
                <p class="mb-2"><i class="fa fa-map-marker-alt text-primary mr-3"></i>
                    @if (!empty($about))
                        {{ $about->address }}
                    @endif
                </p>
                <p class="mb-2"><i class="fa fa-envelope text-primary mr-3"></i>
                    @if (!empty($about))
                        {{ $about->email }}
                    @endif
                </p>
                <p class="mb-0"><i class="fa fa-phone-alt text-primary mr-3"></i>
                    @if (!empty($about))
                        {{ $about->phone }}
                    @endif
                </p>
            </div>
            <div class="col-lg-8 col-md-12">
                <div class="row">
                    <div class="col-md-4 mb-5">
                        <h5 class="text-secondary text-uppercase mb-4"><i class="fas fa-list"></i> ເມນູ Website</h5>
                        <div class="d-flex flex-column justify-content-start">
                            <a class="text-secondary mb-2" href="#"><i
                                    class="fa fa-angle-right mr-2"></i>ໜ້າຫຼັກ</a>
                            <a class="text-secondary mb-2" href="#"><i
                                    class="fa fa-angle-right mr-2"></i>ຮ້ານຄ້າ</a>
                            <a class="text-secondary mb-2" href="#"><i
                                    class="fa fa-angle-right mr-2"></i>ກ່ຽວກັບ</a>
                            <a class="text-secondary mb-2" href="#"><i
                                    class="fa fa-angle-right mr-2"></i>ຕິດຕໍ່ພວກເຮົາ</a>
                            <a class="text-secondary mb-2" href="#"><i
                                    class="fa fa-angle-right mr-2"></i>ສິ່ງທີ່ມັກ</a>
                            <a class="text-secondary" href="#"><i
                                    class="fa fa-angle-right mr-2"></i>ກະຕ່າສິນຄ້າ</a>
                        </div>
                    </div>
                    <div class="col-md-4 mb-5">
                        <h5 class="text-secondary text-uppercase mb-4"><i class="fas fa-address-book"></i> ກ່ຽວກັບຜູ້ໃຊ້</h5>
                        <div class="d-flex flex-column justify-content-start">
                            <a class="text-secondary mb-2" href="#"><i class="fa fa-angle-right mr-2"></i>ລົງທະບຽນ</a>
                            <a class="text-secondary mb-2" href="#"><i class="fa fa-angle-right mr-2"></i>ເຂົ້າສູ່ລະບົບ</a>
                            <a class="text-secondary mb-2" href="#"><i class="fa fa-angle-right mr-2"></i>ໂປຣຟາຍຜູ້ໃຊ້</a>
                            <a class="text-secondary mb-2" href="#"><i class="fa fa-angle-right mr-2"></i>ອອກຈາກລະບົບ
                                </a>
                        </div>
                    </div>
                    <div class="col-md-4 mb-5">
                        <h5 class="text-secondary text-uppercase mb-4"><i class="fas fa-phone-alt"></i> ຊ່ອງທາງການຕິດຕໍ່</h5>
                        <form action="">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="ອີເມວທ່ານ:">
                                <div class="input-group-append">
                                    <button class="btn btn-primary"><i class="fas fa-location-arrow"></i> ສົ່ງໄປ</button>
                                </div>
                            </div>
                        </form>
                        <h6 class="text-secondary text-uppercase mt-4 mb-3">ຕິດຕາມພວກເຮົາ</h6>
                        <div class="d-flex">
                            <a class="btn btn-primary btn-square mr-2" href="#"><i class="fab fa-twitter"></i></a>
                            <a class="btn btn-primary btn-square mr-2" href="#"><i
                                    class="fab fa-facebook-f"></i></a>
                            <a class="btn btn-primary btn-square mr-2" href="#"><i
                                    class="fab fa-linkedin-in"></i></a>
                            <a class="btn btn-primary btn-square" href="#"><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row border-top mx-xl-5 py-4" style="border-color: rgba(256, 256, 256, .1) !important;">
            <div class="col-md-8 px-xl-0">
                <p class="mb-md-0 text-center text-md-left text-secondary">
                    &copy; ອ້າງອີງຈາກ
                    <a class="text-primary" href="https://htmlcodex.com">HTML Codex</a> ອອກແບບ ເເລະ ພັດທະນາຕໍ່ໂດຍນັກສຶກສາ 2023 - 2024
                </p>
            </div>
            <div class="col-md-6 px-xl-0 text-center text-md-right">
                <img class="img-fluid" src="img/payments.png" alt="">
            </div>
        </div>
    </div>
    <!-- Footer End -->
</div>
