<div wire:poll>
    <!-- Topbar Start -->
    <div class="container-fluid">
        <div class="row bg-secondary py-1 px-xl-5">
            <div class="col-lg-6 d-none d-lg-block">
                {{-- <div class="d-inline-flex align-items-center h-100">
                        <a class="text-body mr-3" href="">About</a>
                        <a class="text-body mr-3" href="">Contact</a>
                        <a class="text-body mr-3" href="">Help</a>
                        <a class="text-body mr-3" href="">FAQs</a>
                    </div> --}}
            </div>
            <div class="col-lg-6 text-right text-lg-right">
                {{-- <div class="d-inline-flex align-items-center">
                    @auth
                        <div class="btn-group">
                            <button type="button" class="btn btn-sm btn-light dropdown-toggle" data-toggle="dropdown">Hi! <i
                                    class="fas fa-user"></i> {{ auth()->user()->name_lastname }}</button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="{{ route('frontend.profiles') }}" class="dropdown-item" type="button"><i
                                        class="fas fa-user"></i> ໂປຣຟາຍ</a>
                                <a href="{{ route('frontend.signout') }}" class="dropdown-item text-danger"
                                    type="button"><i class="fas fa-sign-out-alt"></i> ອອກລະບົບ</a>
                            </div>
                        </div>
                    @else
                        <div class="btn-group">
                            <a href="{{ route('frontend.SignIn') }}" class="btn btn-sm btn-light"><i
                                    class="fas fa-user-lock"></i> ເຂົ້າລະບົບ</a>
                            <a href="{{ route('frontend.SignUps') }}" class="btn btn-sm btn-light"><i
                                    class="fas fa-user-edit"></i> ລົງທະບຽນ</a>
                        </div>
                    @endauth
                    <div class="btn-group mx-2">
                            <button type="button" class="btn btn-sm btn-light dropdown-toggle" data-toggle="dropdown">USD</button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <button class="dropdown-item" type="button">EUR</button>
                                <button class="dropdown-item" type="button">GBP</button>
                                <button class="dropdown-item" type="button">CAD</button>
                            </div>
                        </div>
                    <div class="btn-group">
                            <button type="button" class="btn btn-sm btn-light dropdown-toggle" data-toggle="dropdown">EN</button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <button class="dropdown-item" type="button">FR</button>
                                <button class="dropdown-item" type="button">AR</button>
                                <button class="dropdown-item" type="button">RU</button>
                            </div>
                        </div>
                </div> --}}
                <div class="d-inline-flex align-items-center d-block d-lg-none">
                    <a href="{{ route('frontend.ShopCart') }}" class="btn px-0 ml-2">
                        <i class="fas fa-shopping-cart text-dark"></i>
                        <span class="badge text-dark border border-dark rounded-circle" style="padding-bottom: 2px;">
                            @if (!empty($count_shop_cart))
                                {{ $count_shop_cart }}
                                @else
                                0
                            @endif
                        </span>
                    </a>
                    <a href="{{ route('frontend.WishList') }}" class="btn px-0 ml-2">
                        <i class="fas fa-heart text-dark"></i>
                        <span class="badge text-dark border border-dark rounded-circle"
                            style="padding-bottom: 2px;">
                            @if (!empty($count_wishLists))
                            {{ $count_wishLists }}
                            @else
                            0
                        @endif
                        </span>
                    </a>
                    @auth
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-light dropdown-toggle" data-toggle="dropdown">Hi! <i
                                class="fas fa-user"></i> {{ auth()->user()->name_lastname }}</button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="{{ route('frontend.profiles') }}" class="dropdown-item" type="button"><i
                                    class="fas fa-user"></i> ໂປຣຟາຍ</a>
                            <a href="{{ route('frontend.signout') }}" class="dropdown-item text-danger"
                                type="button"><i class="fas fa-sign-out-alt"></i> ອອກລະບົບ</a>
                        </div>
                    </div>
                @else
                    <div class="btn-group">
                        <a href="{{ route('frontend.SignIn') }}" class="btn btn-sm btn-light"><i
                                class="fas fa-user-lock"></i> ເຂົ້າລະບົບ</a>
                        <a href="{{ route('frontend.SignUps') }}" class="btn btn-sm btn-light"><i
                                class="fas fa-user-edit"></i> ລົງທະບຽນ</a>
                    </div>
                @endauth
                </div>
            </div>
        </div>
        <div class="row align-items-center bg-light py-3 px-xl-5 d-none d-lg-flex">
            <div class="col-lg-4">
                <a href="{{ route('frontend.home') }}" class="text-decoration-none">
                    {{-- <span class="h1 text-uppercase text-primary bg-dark px-2">Multi</span> --}}
                    <span class="h2 text-uppercase text-dark text-bold px-2 ml-n1">
                        @if (!empty($about))
                            <img src="{{ asset($about->logo) }}" width="70px" style="border-radius: 100%;" height="70px" alt="" srcset=""> {{ $about->name_la }}
                        @endif
                    </span>
                </a>
            </div>
            @include('livewire.frontend.action-search-content')
            <div class="col-lg-4 col-6 text-right">
                <p class="m-0">ຕິດຕໍ່!</p>
                <h5 class="m-0">
                    @if (!empty($about))
                       <i class="fas fa-phone"></i> {{ $about->phone }}
                    @endif
                </h5>
            </div>
        </div>
    </div>
    <!-- Topbar End -->


    <!-- Navbar Start -->
    <div class="container-fluid mb-30" style="background-color: {{ !empty($about->f_sidebar_color) ? $about->f_sidebar_color : '' }}">
        <div class="row px-xl-5">
            <div class="col-lg-3 d-none d-lg-block">
                {{-- <a class="btn d-flex align-items-center justify-content-between bg-primary w-100" data-toggle="collapse" href="#navbar-vertical" style="height: 65px; padding: 0 30px;">
                        <h6 class="text-dark m-0"><i class="fa fa-bars mr-2"></i>Categories</h6>
                        <i class="fa fa-angle-down text-dark"></i>
                    </a>
                    <nav class="collapse position-absolute navbar navbar-vertical navbar-light align-items-start p-0 bg-light" id="navbar-vertical" style="width: calc(100% - 30px); z-index: 999;">
                        <div class="navbar-nav w-100">
                            <div class="nav-item dropdown dropright">
                                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Dresses <i class="fa fa-angle-right float-right mt-1"></i></a>
                                <div class="dropdown-menu position-absolute rounded-0 border-0 m-0">
                                    <a href="" class="dropdown-item">Men's Dresses</a>
                                    <a href="" class="dropdown-item">Women's Dresses</a>
                                    <a href="" class="dropdown-item">Baby's Dresses</a>
                                </div>
                            </div>
                            <a href="" class="nav-item nav-link">Shirts</a>
                            <a href="" class="nav-item nav-link">Jeans</a>
                            <a href="" class="nav-item nav-link">Swimwear</a>
                            <a href="" class="nav-item nav-link">Sleepwear</a>
                            <a href="" class="nav-item nav-link">Sportswear</a>
                            <a href="" class="nav-item nav-link">Jumpsuits</a>
                            <a href="" class="nav-item nav-link">Blazers</a>
                            <a href="" class="nav-item nav-link">Jackets</a>
                            <a href="" class="nav-item nav-link">Shoes</a>
                        </div>
                    </nav> --}}
            </div>
            <div class="col-lg-9">
                <nav class="navbar navbar-expand-lg navbar-dark py-3 py-lg-0 px-0">
                    <a href="{{ route('frontend.home') }}" class="text-decoration-none d-block d-lg-none">
                        {{-- <span class="h1 text-uppercase text-dark bg-light px-2">Multi</span> --}}
                        <span class="h1 text-uppercase text-light bg-primary px-2 ml-n1">
                            @if (!empty($about))
                                {{ $about->name_la }}
                            @endif
                        </span>
                    </a>
                    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                        <div class="navbar-nav mr-auto py-0">
                            <a href="{{ route('frontend.home') }}" class="nav-item nav-link {{ $currentRoute == 'frontend.home' ? 'active' : '' }}"><i
                                    class="fas fa-home"></i> ໜ້າຫຼັກ</a>
                            <a href="{{ route('frontend.shop') }}" class="nav-item nav-link {{ $currentRoute == 'frontend.shop' ? 'active' : '' }}">ສິນຄ້າ</a>
                            <a href="{{ route('frontend.OrderHistory') }}" class="nav-item nav-link {{ $currentRoute == 'frontend.OrderHistory' ? 'active' : '' }}">ປະຫວັດສັ່ງຊື້</a>
                            <a href="{{ route('frontend.abouts') }}" class="nav-item nav-link {{ $currentRoute == 'frontend.abouts' ? 'active' : '' }}">ກ່ຽວກັບ</a>
                            <a href="{{ route('frontend.contacts') }}" class="nav-item nav-link {{ $currentRoute == 'frontend.contacts' ? 'active' : '' }}">ຕິດຕໍ່ພວກເຮົາ</a>
                            {{-- @auth
                            <a href="{{ route('frontend.profiles') }}" class="nav-item nav-link {{ $currentRoute == 'frontend.profiles' ? 'active' : '' }}"><i
                             class="fas fa-user"></i> {{ auth()->user()->name_lastname }}</a>
                             <a href="{{ route('frontend.signout') }}" class="nav-item nav-link"><i
                                 class="fas fa-sign-out-alt"></i> ອອກລະບົບ</a>
                                @else
                                <a href="{{ route('frontend.SignIn') }}" class="nav-item nav-link {{ $currentRoute == 'frontend.SignIn' ? 'active' : '' }}"><i class="fas fa-user-lock"></i> ເຂົ້າສູ່ລະບົບ</a>
                                <a href="{{ route('frontend.SignUps') }}" class="nav-item nav-link {{ $currentRoute == 'frontend.SignUps' ? 'active' : '' }}"><i class="fas fa-user-edit"></i> ລົງທະບຽນ</a>
                            @endauth --}}
                            {{-- <div class="nav-item dropdown">
                                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Pages <i class="fa fa-angle-down mt-1"></i></a>
                                    <div class="dropdown-menu bg-primary rounded-0 border-0 m-0">
                                        <a href="cart.html" class="dropdown-item">Shopping Cart</a>
                                        <a href="checkout.html" class="dropdown-item">Checkout</a>
                                    </div>
                                </div>
                                <a href="contact.html" class="nav-item nav-link">Contact</a> --}}
                        </div>
                        <div class="navbar-nav ml-auto py-0 d-none d-lg-block">
                           
                            <a href="{{ route('frontend.ShopCart') }}" class="btn px-0 ml-3">
                                <i class="fas fa-shopping-cart text-primary"></i>
                                <span class="badge text-secondary border border-secondary rounded-circle"
                                    style="padding-bottom: 2px;">
                                    @if (!empty($count_shop_cart))
                                        {{ $count_shop_cart }}
                                        @else
                                        0
                                    @endif
                                </span>
                            </a>
                            <a href="{{ route('frontend.WishList') }}" class="btn px-0">
                                <i class="fas fa-heart text-primary"></i>
                                <span class="badge text-secondary border border-secondary rounded-circle"
                                    style="padding-bottom: 2px;">
                                    @if (!empty($count_wishLists))
                                    {{ $count_wishLists }}
                                    @else
                                    0
                                @endif
                            </span>
                            </a>
                            @auth
                            <a href="{{ route('frontend.profiles') }}" class="btn text-white px-0 ml-3 {{ $currentRoute == 'frontend.profiles' ? 'active' : '' }}"><i
                             class="fas fa-user"></i> {{ auth()->user()->name_lastname }}</a>
                             <a href="{{ route('frontend.signout') }}" class="btn text-white px-0 ml-3"><i
                                 class="fas fa-sign-out-alt"></i> ອອກລະບົບ</a>
                                @else
                                <a href="{{ route('frontend.SignIn') }}" class="btn text-white px-0 ml-3 {{ $currentRoute == 'frontend.SignIn' ? 'active' : '' }}"><i class="fas fa-user-lock"></i> ເຂົ້າສູ່ລະບົບ</a>
                                <a href="{{ route('frontend.SignUps') }}" class="btn text-white px-0 ml-3 {{ $currentRoute == 'frontend.SignUps' ? 'active' : '' }}"><i class="fas fa-user-edit"></i> ລົງທະບຽນ</a>
                            @endauth
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <!-- Navbar End -->
</div>
