<div wire:poll>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

                <div class="col-sm-6">
                    <h6><i class="fas fa-cart-plus"></i> ການຂາຍ <i class="fa fa-angle-double-right"></i>
                        ລາຍການລູກຄ້າສັ່ງຊື້ຜ່ານເວບໄຊ</h6>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('backend.dashboard') }}">ໜ້າຫຼັກ</a>
                        </li>
                        <li class="breadcrumb-item active">ລາຍການລູກຄ້າສັ່ງຊື້ຜ່ານເວບໄຊ</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    @foreach ($function_available as $item1)
    @if ($item1->function->name == 'action_4')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="date" wire:model="start_date" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="date" wire:model="end_date" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <input wire:model.live="search" type="text" class="form-control"
                                        placeholder="ຄົ້ນຫາ...">
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select wire:model="status" class="form-control">
                                            <option value="" selected>ເບິ່ງຕາມ-ສະຖານະ</option>
                                            <option value="1">ສັ່ງຊື້ໃຫມ່</option>
                                            <option value="2">ກຳລັງສົ່ງ</option>
                                            <option value="3">ສົ່ງສຳເລັດ</option>
                                            <option value="4">ຍົກເລີກ</option>
                                        </select>
                                    </div>
                                </div>
                                {{-- <div class="col-md-2">
                                    <div class="form-group">
                                        <select wire:model="status" class="form-control">
                                            <option value="" selected>ເລືອກ-ຊຳລະ</option>
                                            <option value="1">ຄ້າງຊຳລະ</option>
                                            <option value="2">ຊຳລະເເລ້ວ</option>
                                        </select>
                                    </div>
                                </div> --}}
                                <div class="col-md-5"></div>
                                <div class="col-md-1">
                                    {{-- <a href="{{route('backend.order_add')}}" type="button" class="btn btn-warning" style="width: auto;"><i class="fa fa-cart-plus"></i> {{__('lang.purchase_orders')}}</a> --}}
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped text-sm text-center">
                                    <thead>
                                        <tr class="text-center bg-light">
                                            <th>ລຳດັບ</th>
                                            <th>ວັນທີ</th>
                                            <th>ລະຫັດ</th>
                                            <th>ລູກຄ້າ</th>
                                            <th>ຍອດລວມ</th>
                                            <th>ປະເພດ</th>
                                            <th>ສະຖານະ</th>
                                            <th>ຈັດການ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $num = 1;
                                        @endphp

                                        @foreach ($data as $item)
                                            <tr>
                                                <td>{{ $num++ }}</td>
                                                <td>{{ date('d/m/Y', strtotime($item->created_at)) }}</td>
                                                <td>{{ $item->code }}</td>
                                                <td>
                                                    @if (!empty($item->customer))
                                                        {{ $item->customer->name_lastname }} <br>
                                                        {{ $item->customer->phone }}
                                                    @endif
                                                </td>
                                                <td class="text-primary">
                                                    {{ number_format($item->sales_detail_sum_subtotal) }}
                                                    ₭</td>
                                                <td>
                                                    @if ($item->type == 1)
                                                        <p class="text-success"><i class="fas fa-hand-holding-usd"></i>
                                                            ເງິນສົດ</p>
                                                    @elseif($item->type == 2)
                                                        <button class="btn btn-outline-danger btn-sm"
                                                            wire:click='showOnepay({{ $item->id }})'><i
                                                                class="fas fa-money"></i> ເງິນໂອນ</button>
                                                    @elseif($item->type == 3)
                                                        <button class="btn btn-outline-success btn-sm"
                                                            wire:click='showOnepay({{ $item->id }})'><i
                                                                class="fas fa-check-circle"></i> ໂອນເເລ້ວ</button>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($item->status == 1)
                                                        <span class="text-success"><i class="fas fa-plus"></i>
                                                            ໃຫມ່</span>
                                                    @elseif($item->status == 2)
                                                        <span class="text-warning"><i class="fas fa-truck-moving"></i>
                                                            ກຳລັງສົ່ງ</span>
                                                    @elseif($item->status == 3)
                                                        <span class="text-success"><i class="fas fa-check-circle"></i>
                                                            ສົ່ງສຳເລັດ</span>
                                                            @elseif($item->status == 4)
                                                        <span class="text-danger"><i class="fas fa-times-circle"></i>
                                                            ຍົກເລີກ</span>
                                                    @endif
                                                </td>

                                                {{-- <td>
                                                    @if (!empty($item->employee))
                                                        {{ $item->employee->name_lastname }}
                                                    @endif
                                                </td> --}}
                                                <td width="2%" style="text-align: center">
                                                    <div wire:ignore class="btn-group btn-left">
                                                        @if($item->status != 4)
                                                        <button type="button"
                                                            class="btn btn-info btn-sm dropdown-toggle dropdown-icon"
                                                            data-toggle="dropdown">ຈັດການ
                                                        </button>
                                                        @else>
                                                        {{ $item->note }}
                                                        @endif
                                                        <div class="dropdown-menu" role="menu">
                                                            @if ($item->status != 1 && $item->status != 3)
                                                                <a class="dropdown-item" href="javascript:void(0)"
                                                                    wire:click="ConfitmSuccess({{ $item->id }})"><i
                                                                        class="fas fa-check-circle">
                                                                    </i> ຍືນຍັນສົ່ງສຳເລັດ</a>
                                                            @endif
                                                            @if ($item->type == 3 && $item->status != 2 && $item->status != 3)
                                                                <a class="dropdown-item" href="javascript:void(0)"
                                                                    wire:click="ShowShipping({{ $item->id }})"><i
                                                                        class="fas fa-truck-moving">
                                                                    </i> ຈັດສົ່ງສິນຄ້າ</a>
                                                            @elseif ($item->type == 1 && $item->status != 2 && $item->status != 3)
                                                                <a class="dropdown-item" href="javascript:void(0)"
                                                                    wire:click="ShowShipping({{ $item->id }})"><i
                                                                        class="fas fa-truck-moving">
                                                                    </i> ຈັດສົ່ງສິນຄ້າ</a>
                                                            @endif
                                                            <a class="dropdown-item" href="javascript:void(0)"
                                                                wire:click="ShowBill({{ $item->id }})"><i
                                                                    class="fas fa-print">
                                                                </i> ພິມບິນ</a>
                                                            </a>
                                                            <a class="dropdown-item"
                                                                wire:click='ShowUpdate({{ $item->id }})'
                                                                href="javascript:void(0)"><i class="fas fa-edit"></i>
                                                                ແກ້ໄຂ</a>
                                                            <a class="dropdown-item"
                                                                wire:click='showDestory({{ $item->id }})'
                                                                href="javascript:void(0)"><i class="fas fa-trash"></i>
                                                                ລຶບອອກ</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                                <div class="float-right">
                                    {{-- {{ $data->links() }} --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
    @endforeach
    <div wire:ignore.self class="modal fabe" id="modal-onepay">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h4 class="modal-title"><i class="fas fa-credit-card"> </i> ຫຼັກຖານການໂອນ</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img src="{{ asset($this->onepay) }}" alt="" width="100%" height="400px">
                </div>
                @if (!($this->type == 3))
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">ປິດ</button>
                        <button type="button" wire:click='ConfirmOnepay({{ $this->ID }})'
                            class="btn btn-success btn-sm" data-dismiss="modal"><i class="fas fa-check-circle"></i>
                            ຍືນຍັນ</button>
                    </div>
                @endif
            </div>
        </div>
    </div>
    @include('livewire.backend.sales.sales-modal')
    @include('livewire.backend.data-store.modal-script')
</div>
