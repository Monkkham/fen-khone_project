<div>
    <!-- Signup Start -->
    <div class="container-fluid">
        <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span class="bg-secondary pr-3"><i
                    class="fas fa-user-edit"></i> ໂປຣຟາຍຂອງຂ້ອຍ</span></h2>
        <div class="row px-xl-5 justify-content-center">
            <div class="col-lg-8 mb-5">
                <div class="contact-form bg-light p-30">
                    <div id="success"></div>
                    <form name="sentMessage" id="contactForm" novalidate="novalidate">
                        <div class="control-group">
                            <input type="text" wire:model='name_lastname'
                                class="form-control @error('name_lastname') is-invalid @enderror" id="name_lastname"
                                placeholder="ຊື່ ເເລະ ນາມສະກຸນ" required="required"
                                data-validation-required-message="Please enter your name" />
                            @error('name_lastname')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="row">
                            <div class="col-md-6 control-group">
                                <input type="text" wire:model='email'
                                    class="form-control @error('email') is-invalid @enderror" id="email"
                                    placeholder="ອີເມວ" required="required"
                                    data-validation-required-message="Please enter your name" />
                                @error('email')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="col-md-6 control-group">
                                <input type="number" wire:model="phone"
                                    class="form-control @error('phone') is-invalid @enderror" id="number"
                                    min="1" placeholder="ເບີໂທ 8 ຕົວເລກ" required="required"
                                    data-validation-required-message="Please enter your phone" />
                                @error('phone')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <select class="custom-select" wire:model.live='province_id'>
                                    <option selected>ເລືອກ-ແຂວງ</option>
                                    @foreach ($province as $item)
                                    <option value="{{ $item->id }}">{{ $item->name_la }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div wire:ignore.self class="col-md-6 form-group">
                                <select class="custom-select" wire:model.live='district_id'>
                                    <option selected>ເລືອກ-ເມືອງ</option>
                                    @foreach ($districts as $item)
                                    <option value="{{ $item->id }}">{{ $item->name_la }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-12 control-group">
                                <input type="text" wire:model='village_id'
                                    class="form-control @error('village_id') is-invalid @enderror" id="village_id"
                                    placeholder="ບ້ານ" required="required"
                                    data-validation-required-message="Please enter your name" />
                                @error('village_id')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center">
                            <button wire:click='updateProfile' class="btn btn-primary py-2 px-4" type="button">
                                <i class="fas fa-edit"></i> ແກ້ໄຂໂປຣຟາຍ</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row px-xl-5 justify-content-center">
            <div class="col-lg-8 mb-5">
                <div class="contact-form bg-light p-30">
                    <div id="success"></div>
                    <form name="sentMessage" id="contactForm" novalidate="novalidate">
                        <div class="control-group">
                            <input type="password" wire:model=currentPassword
                                class="form-control @error('currentPassword') is-invalid @enderror" id="subject"
                                placeholder="ລະຫັດຜ່ານເກົ່າ" required="required"
                                data-validation-required-message="Please enter a subject" />
                            @error('currentPassword')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="row">
                            <div class="col-md-6 control-group">
                                <input type="password" wire:model=newPassword
                                    class="form-control @error('newPassword') is-invalid @enderror" id="subject"
                                    placeholder="ລະຫັດຜ່ານໃຫມ່" required="required"
                                    data-validation-required-message="Please enter a subject" />
                                @error('newPassword')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="col-md-6 control-group">
                                <input type="password" wire:model='confirmPassword'
                                    class="form-control @error('confirmPassword') is-invalid @enderror" id="subject"
                                    placeholder="ຍືນຍັນລະຫັດຜ່ານຫໃຫມ່" required="required"
                                    data-validation-required-message="Please enter a subject" />
                                @error('confirmPassword')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center">
                            <button wire:click='changePassword' class="btn btn-primary py-2 px-4" type="button">
                                <i class="fas fa-key"></i> ປ່ຽນລະຫັດຜ່ານ</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Contact End -->
</div>
