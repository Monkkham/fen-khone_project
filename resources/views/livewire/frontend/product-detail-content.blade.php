<div>
    <!-- Breadcrumb Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-12">
                <nav class="breadcrumb bg-light mb-30">
                    <a class="breadcrumb-item text-dark" href="{{ route('frontend.home') }}">ໜ້າຫຼັກ</a>
                    <a class="breadcrumb-item text-dark" href="{{ route('frontend.shop') }}">ຮ້ານຄ້າ</a>
                    <span class="breadcrumb-item active">ລາຍລະອຽດສິນຄ້າ</span>
                </nav>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->


    <!-- Shop Detail Start -->
    <div class="container-fluid pb-5">
        <div class="row px-xl-5">
            <div class="col-lg-5 mb-30">
                <div id="product-carousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner bg-light">
                        <div class="carousel-item active">
                            <img class="w-100 h-100" src="{{ asset($this->image) }}" alt="Image">
                        </div>
                        <div class="carousel-item">
                            <img class="w-100 h-100" src="{{ asset($this->image) }}" alt="Image">
                        </div>
                        <div class="carousel-item">
                            <img class="w-100 h-100" src="{{ asset($this->image) }}" alt="Image">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#product-carousel" data-slide="prev">
                        <i class="fa fa-2x fa-angle-left text-dark"></i>
                    </a>
                    <a class="carousel-control-next" href="#product-carousel" data-slide="next">
                        <i class="fa fa-2x fa-angle-right text-dark"></i>
                    </a>
                </div>
            </div>

            <div class="col-lg-7 h-auto mb-30">
                <div class="h-100 bg-light p-30">
                    <h3>{{ $this->name }}</h3>
                    <div class="d-flex mb-3">
                        <div class="text-primary mr-2">
                            <small class="fas fa-star"></small>
                            <small class="fas fa-star"></small>
                            <small class="fas fa-star"></small>
                            <small class="fas fa-star-half-alt"></small>
                            <small class="far fa-star"></small>
                        </div>
                        <small class="pt-1">(99 Reviews)</small>
                    </div>
                    <h3 class="font-weight-semi-bold mb-4">ລາຄາ {{ number_format($this->sell_price) }} ₭</h3>
                    @if($this->promotion_price > 0)
                    <h4 class="font-weight-semi-bold mb-4">ປົກກະຕິ <del class="text-danger">{{ number_format($this->promotion_price) }} ₭</del></h4>
                    @endif
                    <p class="mb-4">
                        {{-- {!! $this->note !!} --}}
                    </p>
                    {{-- <div class="d-flex mb-3">
                        <strong class="text-dark mr-3">ຂະຫນາດ:</strong>
                        <form>
                            @foreach(['XS', 'S', 'M', 'L', 'XL'] as $size)
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" wire:model='sizes' class="custom-control-input" id="size-{{ $size }}" value="{{ $size }}">
                                    <label class="custom-control-label" for="size-{{ $size }}">{{ $size }}</label>
                                </div>
                            @endforeach
                        </form>
                    </div>
                    <div class="d-flex mb-4">
                        <strong class="text-dark mr-3">ສີສັນ:</strong>
                        <form>
                            @foreach(['ດຳ', 'ຂາວ', 'ແດງ', 'ຟ້າ', 'ຂຽວ'] as $color)
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" wire:model='colors' class="custom-control-input" id="color-{{ $color }}"  value="{{ $color }}">
                                    <label class="custom-control-label" for="color-{{ $color }}">{{ $color }}</label>
                                </div>
                            @endforeach
                        </form>
                    </div> --}}
                    {{-- <div class="d-flex mb-4">
                        <strong class="text-dark mr-3">ສີສັນ:</strong>
                        <form>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="color-1" name="color">
                                <label class="custom-control-label" for="color-1">ດຳ</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="color-2" name="color">
                                <label class="custom-control-label" for="color-2">ຂາວ</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="color-3" name="color">
                                <label class="custom-control-label" for="color-3">ແດງ</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="color-4" name="color">
                                <label class="custom-control-label" for="color-4">ຟ້າ</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" class="custom-control-input" id="color-5" name="color">
                                <label class="custom-control-label" for="color-5">ຂຽວ</label>
                            </div>
                        </form>
                    </div> --}}
                    <div class="d-flex align-items-center mb-4 pt-2">
                        <div class="input-group quantity mr-3" style="width: 130px;">
                            <div class="input-group-btn">
                                <button class="btn btn-primary btn-minus">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                            <input type="text" class="form-control bg-secondary border-0 text-center"
                                value="1">
                            <div class="input-group-btn">
                                <button class="btn btn-primary btn-plus">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                        @if($this->stock <= 0)
                        <button disabled wire:click="AddToCart('{{ $this->slug_id }}')" class="btn btn-danger px-3"><i class="fa fa-shopping-cart mr-1"></i> ສິນຄ້າຫມົດ!</button>
                        @else
                        <button wire:click="AddToCart('{{ $this->slug_id }}')" class="btn btn-primary px-3"><i class="fa fa-shopping-cart mr-1"></i> ເກັບໃສ່ກະຕ່າ</button>
                        @endif
                        
                    </div>
                    <div class="d-flex pt-2">
                        <strong class="text-dark mr-2">Share on:</strong>
                        <div class="d-inline-flex">
                            <a class="text-dark px-2" href="">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a class="text-dark px-2" href="">
                                <i class="fab fa-twitter"></i>
                            </a>
                            <a class="text-dark px-2" href="">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                            <a class="text-dark px-2" href="">
                                <i class="fab fa-pinterest"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row px-xl-5">
            <div class="col">
                <div class="bg-light p-30">
                    <div class="nav nav-tabs mb-4">
                        <a class="nav-item nav-link text-dark active" data-toggle="tab"
                            href="#tab-pane-1">ລາຍລະອຽດສິນຄ້າ</a>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="tab-pane-1">
                            <p>{!! $this->note !!}</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Shop Detail End -->

    <!-- Products start -->
    <div class="container-fluid py-5">
        <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span
                class="bg-secondary pr-3">ສິນຄ້າທີ່ໃກ້ຄຽງ</span></h2>
        <div class="row px-xl-5">
            <div class="col">
                <div class="owl-carousel related-carousel">
                    @foreach ($products as $item)
                    <div class="product-item bg-light">
                        <div class="product-img position-relative overflow-hidden">
                            <img style="height: 280px; width:100%" class="img-fluid w-100" src="{{ asset($item->image) }}" alt="">
                            <div class="product-action">
                                <a wire:click='AddToCart({{ $item->id }})' class="btn btn-outline-dark btn-square" href="#"><i
                                        class="fa fa-shopping-cart"></i></a>
                                <a wire:click='AddToWishList({{ $item->id }})' class="btn btn-outline-dark btn-square" href=""><i
                                        class="far fa-heart"></i></a>
                            </div>
                        </div>
                        <div class="text-center py-4">
                            <a href="#" class="h6 text-decoration-none text-truncate" wire:click='ProductDetail({{ $item->id }})'><div class="text-truncate" style="max-width: 200px;">{{ $item->name }}</div>
                            <div class="d-flex align-items-center justify-content-center mt-2">
                                <h5>{{ number_format($item->sell_price) }} ₭</h5>
                            @if($item->promotion_price > 0)
                                <h6 class="text-muted ml-2"><del class="text-danger">{{ number_format($item->promotion_price) }} ₭</del></h6>
                            @endif
                            </div>
                        </a>
                            <div class="d-flex align-items-center justify-content-center mb-1">
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small class="fa fa-star text-primary mr-1"></small>
                                <small>(99)</small>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- Products End -->
</div>
