<div>
    <!-- Breadcrumb Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-12">
                <nav class="breadcrumb bg-light mb-30">
                    <a class="breadcrumb-item text-dark" href="#">ໜ້າຫຼັກ</a>
                    <a class="breadcrumb-item text-dark" href="#">ຮ້ານຄ້າ</a>
                    <span class="breadcrumb-item active">ການຊຳລະເງິນ</span>
                </nav>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->


    <!-- Checkout Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-lg-8">
                <h5 class="section-title position-relative text-uppercase mb-3"><span class="bg-secondary pr-3"><i
                            class="fas fa-address-book"></i> ກວດສອບຂໍ້ມູນທ່ານໃຫ້ຄົບຖ້ວນ</span></h5>
                <div class="bg-light p-30 mb-5">
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label>ຊື່ ເເລະ ນາມສະກຸນ</label>
                            <input type="text" wire:model='name_lastname'
                                class="form-control @error('name_lastname') is-invalid @enderror" id="name_lastname"
                                placeholder="ຊື່ ເເລະ ນາມສະກຸນ" required="required"
                                data-validation-required-message="Please enter your name" />
                            @error('name_lastname')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-md-6 form-group">
                            <label>ເບີໂທ</label>
                            <input type="number" wire:model='phone'
                                class="form-control @error('phone') is-invalid @enderror" id="phone"
                                placeholder="ຊື່ ເເລະ ນາມສະກຸນ" required="required"
                                data-validation-required-message="Please enter your name" />
                            @error('phone')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-md-6 form-group">
                            <label>ອີເມວ</label>
                            <input type="ຳທັຮສ" wire:model='email'
                                class="form-control @error('email') is-invalid @enderror" id="email"
                                placeholder="ຊື່ ເເລະ ນາມສະກຸນ" required="required"
                                data-validation-required-message="Please enter your name" />
                            @error('email')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div wire:ignore.self class="col-md-6 form-group">
                            <label>ເພດ</label>
                            <select class="custom-select @error('gender') is-invalid @enderror" id="gender"
                                wire:model.live='gender'>
                                <option value="">ເລືອກ-ເພດ</option>
                                <option value="1">ຍິງ</option>
                                <option value="2">ຊາຍ</option>
                            </select>
                            @error('gender')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-md-6 form-group">
                            <label>ແຂວງ</label>
                            <select class="custom-select @error('province_id') is-invalid @enderror" id="province_id"
                                wire:model.live='province_id'>
                                <option selected>ເລືອກ-ແຂວງ</option>
                                @foreach ($province as $item)
                                    <option value="{{ $item->id }}">{{ $item->name_la }}</option>
                                @endforeach
                            </select>
                            @error('province_id')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div wire:ignore.self class="col-md-6 form-group">
                            <label>ເມືອງ</label>
                            <select class="custom-select @error('district_id') is-invalid @enderror" id="district_id"
                                wire:model.live='district_id'>
                                <option selected>ເລືອກ-ເມືອງ</option>
                                @foreach ($districts as $item)
                                    <option value="{{ $item->id }}">{{ $item->name_la }}</option>
                                @endforeach
                            </select>
                            @error('district_id')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-md-12 control-group">
                            <label>ບ້ານ (ຖະຫນົນເລກທີ່)</label>
                            <input type="text" wire:model='village_id'
                                class="form-control @error('village_id') is-invalid @enderror" id="village_id"
                                placeholder="ບ້ານ" required="required"
                                data-validation-required-message="Please enter your name" />
                            @error('village_id')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                            <p class="help-block text-danger"></p>
                        </div>
                        <div wire:ignore.self class="col-md-4 form-group">
                            <label>ເລືອກປະເພດ</label>
                            <select class="custom-select @error('doc_type') is-invalid @enderror" id="doc_type"
                                wire:model.live='doc_type'>
                                <option value="">ເລືອກ-ປະເພດ</option>
                                <option value="1">ສຳມະໂນຄົວ</option>
                                <option value="2">ບັດປະຈຳຕົວ</option>
                            </select>
                            @error('doc_type')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-md-4 control-group">
                            <label>ອອກຊື່-
                            @if($this->doc_type == 1)
                                ສໍາມະໂນຄົວ
                                @elseif($this->doc_type == 2)
                                ບັດປະຈຳຕົວ
                            @endif</label>
                            <input type="text" wire:model='name_doc'
                                class="form-control @error('name_doc') is-invalid @enderror" id="name_doc"
                                placeholder="ປ້ອນຂໍ້ມູນ" required="required"
                                data-validation-required-message="Please enter your name" />
                            @error('name_doc')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="col-md-4 control-group">
                            <label>ເລກທີ-@if($this->doc_type == 1)
                                ສໍາມະໂນຄົວ
                                @elseif($this->doc_type == 2)
                                ບັດປະຈຳຕົວ
                            @endif</label>
                            <input type="text" wire:model='number_doc'
                                class="form-control @error('number_doc') is-invalid @enderror" id="number_doc"
                                placeholder="ປ້ອນຂໍ້ມູນ" required="required"
                                data-validation-required-message="Please enter your name" />
                            @error('number_doc')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                            <p class="help-block text-danger"></p>
                        </div>
                        {{-- <div class="col-md-12 form-group">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="newaccount">
                                <label class="custom-control-label" for="newaccount">Create an account</label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="shipto">
                                <label class="custom-control-label" for="shipto" data-toggle="collapse"
                                    data-target="#shipping-address">Ship to different address</label>
                            </div>
                        </div> --}}
                    </div>
                </div>
                {{-- <div class="collapse mb-5" id="shipping-address">
                    <h5 class="section-title position-relative text-uppercase mb-3"><span
                            class="bg-secondary pr-3">Shipping Address</span></h5>
                    <div class="bg-light p-30">
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label>First Name</label>
                                <input class="form-control" type="text" placeholder="John">
                            </div>
                            <div class="col-md-6 form-group">
                                <label>Last Name</label>
                                <input class="form-control" type="text" placeholder="Doe">
                            </div>
                            <div class="col-md-6 form-group">
                                <label>E-mail</label>
                                <input class="form-control" type="text" placeholder="example@email.com">
                            </div>
                            <div class="col-md-6 form-group">
                                <label>Mobile No</label>
                                <input class="form-control" type="text" placeholder="+123 456 789">
                            </div>
                            <div class="col-md-6 form-group">
                                <label>Address Line 1</label>
                                <input class="form-control" type="text" placeholder="123 Street">
                            </div>
                            <div class="col-md-6 form-group">
                                <label>Address Line 2</label>
                                <input class="form-control" type="text" placeholder="123 Street">
                            </div>
                            <div class="col-md-6 form-group">
                                <label>Country</label>
                                <select class="custom-select">
                                    <option selected>United States</option>
                                    <option>Afghanistan</option>
                                    <option>Albania</option>
                                    <option>Algeria</option>
                                </select>
                            </div>
                            <div class="col-md-6 form-group">
                                <label>City</label>
                                <input class="form-control" type="text" placeholder="New York">
                            </div>
                            <div class="col-md-6 form-group">
                                <label>State</label>
                                <input class="form-control" type="text" placeholder="New York">
                            </div>
                            <div class="col-md-6 form-group">
                                <label>ZIP Code</label>
                                <input class="form-control" type="text" placeholder="123">
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
            <div class="col-lg-4">
                <h5 class="section-title position-relative text-uppercase mb-3"><span class="bg-secondary pr-3"><i
                            class="fas fa-cart-plus"></i> ລາຍການສັ່ງຊື້</span></h5>
                @php
                    $num = 1;
                @endphp
                <div class="bg-light p-30 mb-5">
                    <div class="border-bottom">
                        <h6 class="mb-3 text-bold">ລາຍການສິນຄ້າທີ່ເລືອກ</h6>
                        @foreach ($shop_cart as $item)
                            <div class="d-flex justify-content-between">
                                <p>{{ $num++ }}. {{ $item->name }}</p>
                                <p>x{{ $item->qty }}</p>
                                <p>{{ number_format($item->price) }} ₭</p>
                            </div>
                        @endforeach
                    </div>
                    <div class="border-bottom pt-3 pb-2">
                        <div class="d-flex justify-content-between mb-3">
                            <h6><b>ເປັນເງິນ</b></h6>
                            <h6><b>{{ number_format($sum_subtotals) }} ₭</b></h6>
                        </div>
                        <div class="d-flex justify-content-between">
                            <h6 class="font-weight-medium"><b>ຄ່າຂົນສົ່ງ</b></h6>
                            <h6 class="font-weight-medium"><b>Free</b></h6>
                        </div>
                    </div>
                    <div class="pt-2">
                        <div class="d-flex justify-content-between mt-2">
                            <h5><b>ລວມທັງຫມົດ</b></h5>
                            <h5><b>{{ number_format($sum_subtotals) }} ₭</b></h5>
                        </div>
                    </div>
                </div>
                <div class="mb-5">
                    <h5 class="section-title position-relative text-uppercase mb-3"><span
                            class="bg-secondary pr-3">ປະເພດການຊຳລະ</span></h5>
                    <div class="bg-light p-30">
                        <div class="form-group">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" name="payment" id="directcheck"
                                    value="1" wire:model="type">
                                <label class="custom-control-label" for="directcheck">ເງິນສົດ (COD ເກັບປາຍທາງ)</label>
                            </div>
                        </div>
                        <div class="form-group mb-4">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" name="payment" id="banktransfer"
                                    value="2" wire:model="type">
                                <label class="custom-control-label" for="banktransfer">ເງິນໂອນ (ຊໍາລະຕົ້ນທາງ)</label>
                            </div>
                            @if ($this->type == 2)
                                @if ($this->onepay)
                                    <img width="100%" height="400px" src="{{ $this->onepay->temporaryUrl() }}"
                                        alt="Description of the image">
                                @else
                                    <img width="100%" height="400px"
                                        src="
                                        @if (!empty($about)) {{ asset($about->image) }} @endif"
                                        alt="Description of the image">
                                @endif
                                @if (!$this->onepay)
                                    <input type="file" wire:model='onepay'
                                        class="@error('onepay') is-invalid @enderror" accept=".png, .jpg, .jpeg">
                                    @error('onepay')
                                        <span style="color: red" class="error">{{ $message }}</span>
                                    @enderror
                                @endif
                            @endif

                        </div>
                        <button wire:click='PlaceSales()' class="btn btn-block btn-primary font-weight-bold py-3"><i
                                class="fas fa-credit-card"></i> ຍືນຍັນຊຳລະ</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Checkout End -->
</div>
