<div>
    <!-- Signup Start -->
    <div class="container-fluid">
        <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span class="bg-secondary pr-3"><i
                    class="fas fa-user-lock"></i> ເຂົ້າສູ່ລະບົບ</span></h2>
        <div class="row px-xl-5 justify-content-center">
            <div class="col-lg-8 mb-5">
                <div class="contact-form bg-light p-30">
                    <div id="success"></div>
                    <form name="sentMessage" id="contactForm" novalidate="novalidate">
                        <div class="control-group">
                            <label>ເບີໂທ</label>
                            <input type="number" wire:model="phone"
                                class="form-control @error('phone') is-invalid @enderror" id="number" min="1"
                                placeholder="ເບີໂທ 8 ຕົວເລກ" required="required"
                                data-validation-required-message="Please enter your phone" />
                            @error('phone')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="control-group">
                            <label>ລະຫັດຜ່ານ</label>
                            <input type="password" wire:model=password
                                class="form-control @error('password') is-invalid @enderror" id="subject"
                                placeholder="ລະຫັດຜ່ານ" required="required"
                                data-validation-required-message="Please enter a subject" />
                            @error('password')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="col-md-12 form-group">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" wire:model="remember" id="newaccount">
                                <label class="custom-control-label" for="newaccount">ຈຶ່ຈຳຂ້ອຍໄວ້</label>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center">
                            <button wire:click='Signin' class="btn btn-primary py-2 px-4" type="button">
                                <i class="fas fa-sign-in-alt"></i> ເຂົ້າສູ່ລະບົບ</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Contact End -->
    @include('layouts.backend.script')
</div>
