<div>
    <!-- Breadcrumb Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-12">
                <nav class="breadcrumb bg-light mb-30">
                    <a class="breadcrumb-item text-dark" href="#">ໜ້າຫຼັກ</a>
                    <a class="breadcrumb-item text-dark" href="#">ຮ້ານຄ້າ</a>
                    <span class="breadcrumb-item active">ສິ່ງທີ່ມັກ</span>
                </nav>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->
    <!-- Cart Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-lg-12 table-responsive mb-5">
                <table class="table table-light table-borderless table-hover text-center mb-0">
                    <thead class="text-white" style="background-color: {{ !empty($about->f_sidebar_color) ? $about->f_sidebar_color : '' }}">
                        <tr>
                            <th>ລໍາດັບ</th>
                            <th>ສິນຄ້່າ</th>
                            <th>ລາຄາ</th>
                            <th>ຈັດການ</th>
                        </tr>
                    </thead>
                    @php
                        $num = 1;
                    @endphp
                    <tbody class="align-middle">
                        @if (count($wishLists) > 0)
                            @foreach ($wishLists as $item)
                                <tr>
                                    <td>{{ $num++ }}</td>
                                    <td class="align-middle"><img src="img/product-1.jpg" alt=""
                                            style="width: 50px;">
                                        {{ $item->name }}</td>
                                    <td class="align-middle"> {{ number_format($item->price) }} ₭</td>
                                    <td class="align-middle"><button wire:click='AddToCart({{ $item->id }})'
                                            class="btn btn-sm btn-success"><i class="fas fa-cart-plus"></i> ເພີ່ມໃສ່ກະຕ່າ</button>
                                            <button wire:click='Remove_Item({{ $item->id }})'
                                            class="btn btn-sm btn-danger"><i class="fas fa-times-circle"></i></button></td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6">
                                    <span class="text-danger"><i class="fas fa-box-open"></i> ບໍ່ມີສິນຄ້າໃນລາຍການທີ່ມັກ
                                    </span><a href="{{ route('frontend.shop') }}"> ໄປທີ່ຮ້ານຄ້າ <i
                                            class="fas fa-arrow-right"></i></a>
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Cart End -->
</div>
